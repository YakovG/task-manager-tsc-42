package ru.goloshchapov.tm.constant;

public interface FieldConst {

    String ID = "id";

    String TIMESTAMP = "timestamp";

    String USER_ID = "userId";

    String SIGNATURE = "signature";

    String LOGIN = "login";

    String PASSWORDHASH = "passwordHash";

    String EMAIL = "email";

    String FIRSTNAME = "firstname";

    String LASTNAME = "lastname";

    String MIDDLENAME = "middlename";

    String ROLE = "Role";

    String LOCKED = "locked";

    String NAME = "name";

    String DESCRIPTION = "description";

    String STATUS = "status";

    String CREATED = "created";

    String DATE_START = "dateStart";

    String DATE_FINISH = "dateFinish";

    String PROJECT_ID = "projectId";
}
