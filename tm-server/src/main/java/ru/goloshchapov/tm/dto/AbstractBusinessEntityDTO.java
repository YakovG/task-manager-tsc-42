package ru.goloshchapov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.entity.IWBS;
import ru.goloshchapov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractBusinessEntityDTO extends AbstractEntityDTO implements IWBS {

    @Nullable
    @Column(name = "user_id")
    private String userId;

    @Column
    @Nullable
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_date")
    private Date dateStart;

    @Nullable
    @Column(name = "finish_date")
    private Date dateFinish;

    @Column
    @NotNull
    private Date created = new Date();

    @NotNull
    public AbstractBusinessEntityDTO(@NotNull final String userId) {
        this.userId = userId;
    }

    public boolean checkUserAccess(@NotNull final String userId) {
        return userId.equals(getUserId());
    }

    @NotNull
    @Override
    public String toString() { return getId() + ". " + name; }

}
