package ru.goloshchapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tm_user")
@NoArgsConstructor
public class UserDTO extends AbstractEntityDTO {

    @Column
    @NotNull
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstname;

    @Nullable
    @Column(name = "last_name")
    private String lastname;

    @Nullable
    @Column(name = "middle_name")
    private String middlename;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Column
    private boolean locked = false;

}
