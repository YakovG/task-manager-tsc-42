package ru.goloshchapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tm_project")
@NoArgsConstructor
public class ProjectDTO extends AbstractBusinessEntityDTO {

}
