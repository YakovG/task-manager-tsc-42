package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.dto.SessionDTO;
import ru.goloshchapov.tm.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<SessionDTO> {

    List<SessionDTO> getListSession(SessionDTO session);

    @Nullable SessionDTO findSessionByLogin(@Nullable String login);

    List<SessionDTO> findAll();

    @Nullable
    @Override
    abstract SessionDTO findOneById(@Nullable String id);

    void closeSessionByLogin(@Nullable String login, @Nullable String password);

    @Nullable SessionDTO sign(@Nullable SessionDTO session);

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable SessionDTO open(@Nullable String login, @Nullable String password);

    SessionDTO add(@Nullable SessionDTO session);

    void close(@Nullable SessionDTO session);

    SessionDTO removeOneById(@Nullable String id);

    boolean isValid(@Nullable SessionDTO session);

    void validate (@Nullable SessionDTO session);

    void validate(@Nullable SessionDTO session, @Nullable Role role);
}
