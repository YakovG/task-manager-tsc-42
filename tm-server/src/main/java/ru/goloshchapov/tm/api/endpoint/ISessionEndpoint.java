package ru.goloshchapov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.ResultDTO;
import ru.goloshchapov.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {
    @WebMethod
    @Nullable SessionDTO openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );

    @WebMethod
    @NotNull ResultDTO closeSession(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @NotNull ResultDTO closeSessionByLogin(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );
}
