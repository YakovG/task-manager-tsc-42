package ru.goloshchapov.tm.api.repository;

import org.apache.ibatis.annotations.Update;

public interface ITestRepository {

    @Update("CREATE TABLE `tm_user` ( " +
            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
            "`login` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`password_hash` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`email` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`first_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`last_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`middle_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`role` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`locked` BIT(1) NULL DEFAULT NULL, " +
            "PRIMARY KEY (`id`) USING BTREE " +
            ") COLLATE='utf8_unicode_ci' ENGINE=InnoDB;")
    void initTestUserTable();

    @Update("CREATE TABLE `tm_session` ( " +
            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
            "`timestamp` BIGINT(20) NOT NULL DEFAULT '0', " +
            "`user_id` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`signature` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "PRIMARY KEY (`id`) USING BTREE, " +
            "INDEX `FK_tm_session_tm_user` (`user_id`) USING BTREE " +
            ") COLLATE='utf8_unicode_ci' ENGINE=InnoDB;")
    void initTestSessionTable();

    @Update("CREATE TABLE `tm_project` ( " +
            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
            "`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`user_id` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`created` TIMESTAMP NULL DEFAULT NULL, " +
            "`start_date` TIMESTAMP NULL DEFAULT NULL, " +
            "`finish_date` TIMESTAMP NULL DEFAULT NULL, " +
            "PRIMARY KEY (`id`) USING BTREE, " +
            "INDEX `FK_tm_project_tm_user` (`user_id`) USING BTREE " +
            ") COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;")
    void initTestProjectTable();

    @Update("CREATE TABLE `tm_task` ( " +
            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
            "`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`user_id` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`project_id` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`created` TIMESTAMP NULL DEFAULT NULL, " +
            "`date_start` TIMESTAMP NULL DEFAULT NULL, " +
            "`date_finish` TIMESTAMP NULL DEFAULT NULL, " +
            "PRIMARY KEY (`id`) USING BTREE, " +
            "INDEX `FK_tm_task_tm_user` (`user_id`) USING BTREE, " +
            "INDEX `FK_tm_task_tm_project` (`project_id`) USING BTREE " +
            ") COLLATE='utf8_unicode_ci' ENGINE=InnoDB;")
    void initTestTaskTable();

    @Update("DROP DATABASE IF EXISTS testtm;")
    void dropDatabase();

}
