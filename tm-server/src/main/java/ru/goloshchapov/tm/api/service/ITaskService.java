package ru.goloshchapov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskService extends IBusinessService<TaskDTO> {

    @Nullable TaskDTO add (String userId, String name, String description);

    @Nullable
    @SneakyThrows
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO task);

    @Nullable
    @SneakyThrows
    TaskDTO update(@Nullable TaskDTO task);

    @SneakyThrows
    boolean isAbsentById(@NotNull String userId, @NotNull String taskId);

    @SneakyThrows
    boolean isAbsentByName(@NotNull String userId, @NotNull String taskName);

    @Nullable
    @SneakyThrows
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<TaskDTO> findAllByUserId(@Nullable String userId);

    @Nullable
    @SneakyThrows
    TaskDTO findOneById(@Nullable String userId, @Nullable String taskId);

    @Nullable
    @SneakyThrows
    TaskDTO findOneByName(@Nullable String name);

    @Nullable
    @SneakyThrows
    TaskDTO findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    @SneakyThrows
    TaskDTO removeOneById(@Nullable String userId, @Nullable String taskId);

    @SneakyThrows
    void clear(@Nullable String userId);

}
