package ru.goloshchapov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.dto.UserDTO;
import ru.goloshchapov.tm.enumerated.Role;

import java.util.List;

public interface IUserService extends IService<UserDTO> {

    UserDTO getRootUser();

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    @Nullable
    @SneakyThrows
    UserDTO create(@Nullable UserDTO user);

    @Nullable
    UserDTO create(String login, String password);

    @Nullable
    UserDTO create(String login, String password, String email);

    @Nullable
    UserDTO create(String login, String password, Role role);

    @Nullable
    UserDTO create(String login, String password, String email, String role);

    @Nullable UserDTO add(@Nullable UserDTO user);

    @NotNull
    @SneakyThrows
    List<UserDTO> findAll();

    UserDTO findUserById(@Nullable String id);

    UserDTO findOneById(@Nullable String id);

    @NotNull
    UserDTO findUserByLogin(String login);

    @NotNull
    UserDTO findUserByEmail(String email);

    @Nullable
    UserDTO removeUser(UserDTO user);

    @Override
    UserDTO removeOneById(@Nullable String id);

    @Nullable
    UserDTO removeUserByLogin(String login);

    @Nullable
    UserDTO removeUserByEmail(String email);

    @Nullable
    UserDTO setPassword(String userId, String password);

    @Nullable
    UserDTO updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );

    @Nullable
    UserDTO lockUserByLogin(String login);

    @Nullable
    UserDTO unlockUserByLogin(String login);

    @Nullable
    UserDTO lockUserByEmail(String email);

    @Nullable
    UserDTO unlockUserByEmail(String email);

    void createTestUser();
}
