package ru.goloshchapov.tm.api;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.AbstractEntityDTO;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntityDTO> {
    @SneakyThrows
    E add(E entity);

    @Nullable
    @SneakyThrows
    List<E> findAll();

    @SneakyThrows
    E findOneById(@Nullable String id);

    @SneakyThrows
    E removeOneById(@Nullable String id);

    @SneakyThrows
    void addAll(@Nullable Collection<E> collection);

    @SneakyThrows
    E findOneByIndex(@Nullable Integer index);

    @SneakyThrows
    boolean isAbsentById(@Nullable String id);

    @SneakyThrows
    boolean isAbsentByIndex(@Nullable Integer index);

    @Nullable
    @SneakyThrows
    String getIdByIndex(@Nullable Integer index);

    @SneakyThrows
    int size();

    @SneakyThrows
    void clear();

    @SneakyThrows
    void remove(E entity);

    @SneakyThrows
    E removeOneByIndex(@Nullable Integer index);
}
