package ru.goloshchapov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO `tm_session`(`id`,`timestamp`,`user_id`,`signature`) " +
            "VALUES(#{id}, #{timestamp}, #{userId}, #{signature})")
    void add (@NotNull SessionDTO session);

    @Select("SELECT COUNT(*) FROM `tm_session`")
    int contains(@Nullable @Param("sessionId") String sessionId);

    @Delete("DELETE FROM `tm_session` WHERE `id` = #{sessionId}")
    void remove(@NotNull String sessionId);

    @Nullable
    @Select("SELECT * FROM `tm_session` WHERE `user_id` = #{userId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    SessionDTO findByUserId (@Nullable String userId);

    @Nullable
    @Select("SELECT * FROM `tm_session`")
    @Result(column = "user_id", property = "userId")
    List<SessionDTO> findAllSession();

    @Delete("DELETE FROM `tm_session`")
    void clear();

    @Select("SELECT * FROM `tm_session` WHERE `id` = #{id} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    SessionDTO findSessionById(@Nullable final String id);
}
