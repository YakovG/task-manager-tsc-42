package ru.goloshchapov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO `tm_task` (`id`, `name`, `description`, `user_id`, `project_id`, `status`, " +
            "`created`, `start_date`, `finish_date`) " +
            "VALUES(#{id}, #{name}, #{description}, #{userId}, #{projectId}, #{status}, " +
            "#{created}, #{dateStart}, #{dateFinish})")
    void add(@NotNull TaskDTO task);

    @Update("UPDATE `tm_task` SET `name` = #{name}, `description` = #{description}, `user_id` = #{userId}, " +
            "`project_id` = #{projectId}, `status` = #{status}, `created` = #{created}, " +
            "`start_date` = #{dateStart}, `finish_date` = #{dateFinish} WHERE `id` = #{id}")
    void update(@NotNull TaskDTO task);

    @Nullable
    @Select("SELECT * FROM `tm_task`")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    @Result(column = "project_id", property = "projectId")
    List<TaskDTO> findAllTasks();

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `user_id` = #{userId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    @Result(column = "project_id", property = "projectId")
    List<TaskDTO> findAll(@NotNull String userId);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `user_id` = #{userId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    @Result(column = "project_id", property = "projectId")
    List<TaskDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `id` = #{taskId} AND `user_id` = #{userId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    @Result(column = "project_id", property = "projectId")
    TaskDTO findOneById(
            @NotNull final @Param("userId") String userId,
            @NotNull final @Param("taskId") String taskId
    );

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `id` = #{taskId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    @Result(column = "project_id", property = "projectId")
    TaskDTO findTaskById(@NotNull final @Param("taskId") String taskId);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `name` = #{name} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    @Result(column = "project_id", property = "projectId")
    TaskDTO findTaskByName(@NotNull final @Param("name") String name);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `name` = #{name} AND `user_id` = #{userId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    @Result(column = "project_id", property = "projectId")
    TaskDTO findOneByName(
            @NotNull final @Param("userId") String userId,
            @NotNull final @Param("name") String name
    );

    @Delete("DELETE FROM `tm_task` WHERE `id` = #{taskId} AND `user_id` = #{userId}")
    void removeOneById(@NotNull final String userId, @NotNull final String taskId);

    @Delete("DELETE FROM `tm_task` WHERE `id` = #{taskId}")
    void removeTaskById(@NotNull final String taskId);

    @Delete("DELETE FROM `tm_task` WHERE `user_id` = #{userId}")
    void clear(@NotNull final String userId);

    @Delete("DELETE FROM `tm_task`")
    void clearAll();

    @Select("SELECT COUNT(*) FROM `tm_task`")
    int size();

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `user_id` = #{userId} AND `project_id` = #{projectId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "dateStart")
    @Result(column = "finish_date", property = "dateFinish")
    @Result(column = "project_id", property = "projectId")
    List<TaskDTO> findAllByProjectId(
            @Nullable final @Param("userId") String userId,
            @Nullable final @Param("projectId") String projectId
    );

    @Update("UPDATE `tm_task` SET `project_id` = #{projectId} " +
            "WHERE `user_id` = #{userId} AND `id` = #{taskId}")
    void bindToProjectById (
            @Nullable final @Param("userId") String userId,
            @Nullable final @Param("taskId") String taskId,
            @Nullable final @Param("projectId") String projectId
    );

    @Update("UPDATE `tm_task` SET `project_id` = #{projectId} " +
            "WHERE `user_id` = #{userId} AND `id` = #{taskId}")
    void unbindFromProjectById (
            @Nullable final @Param("userId") String userId,
            @Nullable final @Param("taskId") String taskId,
            @Nullable final @Param("projectId") String projectId
    );


    @Delete("DELETE FROM `tm_task` WHERE `project_id` = #{projectId} AND `user_id` = #{userId}")
    void removeAllByProjectId(
            @Nullable final @Param("userId") String userId,
            @Nullable final @Param("projectId") String projectId
    );
}
