package ru.goloshchapov.tm.api;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.AbstractBusinessEntityDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IBusinessService<M extends AbstractBusinessEntityDTO> extends IService<M>{


    void addAll(@Nullable final String userId, @Nullable final Collection<M> collection);

    @Nullable
    List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator);

    @Nullable
    List<M> findAllStarted(@Nullable final String userId, @Nullable final Comparator<M> comparator);

    @Nullable
    List<M> findAllCompleted(@Nullable final String userId, @Nullable final Comparator<M> comparator);

    @Nullable
    List<M> sortedBy(String userId, String sortCheck);

    @Nullable
    M findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    boolean isAbsentByName(@Nullable final String name);

    @SneakyThrows
    boolean isAbsentById(@NotNull String userId, @NotNull String projectId);

    @SneakyThrows
    boolean isAbsentByName(@NotNull String userId, @NotNull String projectName);

    @Nullable
    @SneakyThrows
    String getIdByName(@Nullable String name);

    @SneakyThrows
    int size(@Nullable String userId);

    @SneakyThrows
    void remove(@Nullable String userId, M model);

    @SneakyThrows
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @SneakyThrows
    M removeOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    M updateOneById(String userId, String modelId, String name, String description);

    @Nullable
    M updateOneByIndex(String userId, Integer index, String name, String description);

    @SneakyThrows
    M startOneById(@Nullable String userId, @Nullable String modelId);

    @SneakyThrows
    M startOneByIndex(@Nullable String userId, @Nullable Integer index);

    @SneakyThrows
    M startOneByName(@Nullable String userId, @Nullable String name);

    @SneakyThrows
    M finishOneById(@Nullable String userId, @Nullable String modelId);

    @SneakyThrows
    M finishOneByIndex(@Nullable String userId, @Nullable Integer index);

    @SneakyThrows
    M finishOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    M changeOneStatusById(String userId, String id, String statusChange);

    @Nullable
    M changeOneStatusByName(String userId, String name, String statusChange);

    @Nullable
    M changeOneStatusByIndex(String userId, int index, String statusChange);

}
