package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.ProjectDTO;
import ru.goloshchapov.tm.dto.SessionDTO;
import ru.goloshchapov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskProjectEndpoint {
    @WebMethod
    @SneakyThrows
    @Nullable List<TaskDTO> findAllTaskByProjectId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String projectId
    );

    @WebMethod
    @SneakyThrows
    boolean isEmptyProjectWithTaskById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String projectId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<TaskDTO> findAllTaskByProjectName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectName") String projectName
    );

    @WebMethod
    @SneakyThrows
    boolean isEmptyProjectWithTaskByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectName") String projectName
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<TaskDTO> findAllTaskByProjectIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectIndex") Integer projectIndex
    );

    @WebMethod
    @SneakyThrows
    boolean isEmptyProjectWithTaskByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectIndex") Integer projectIndex
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    TaskDTO bindTaskToProjectById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String taskId,
            @WebParam(name = "projectId") String projectId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    TaskDTO unbindTaskFromProjectById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String taskId,
            @WebParam(name = "projectId") String projectId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<TaskDTO> removeAllTaskByProjectId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String projectId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<TaskDTO> removeAllTaskByProjectName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectName") String projectName
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<TaskDTO> removeAllTaskByProjectIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectIndex") Integer projectIndex
    );

    @WebMethod
    @SneakyThrows
    boolean removeAllTaskByUserId(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @Nullable ProjectDTO removeProjectByIdWithTask(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String projectId
    );

    @WebMethod
    @SneakyThrows
    @Nullable ProjectDTO removeProjectByNameWithTask(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectName") String projectName
    );

    @WebMethod
    @SneakyThrows
    @Nullable ProjectDTO removeProjectByIndexWithTask(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectIndex") Integer projectIndex
    );
}
