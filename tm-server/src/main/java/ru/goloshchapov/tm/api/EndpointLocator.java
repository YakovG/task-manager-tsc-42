package ru.goloshchapov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.endpoint.*;

public interface EndpointLocator {

    @NotNull ICalculatorEndpoint getCalculatorEndpoint();

    @NotNull IProjectEndpoint getProjectEndpoint();

    @NotNull ISessionEndpoint getSessionEndpoint();

    @NotNull ITaskProjectEndpoint getTaskProjectEndpoint();

    @NotNull ITaskEndpoint getTaskEndpoint();

    @NotNull IUserEndpoint getUserEndpoint();

    @NotNull IAdminEndpoint getAdminEndpoint();

}
