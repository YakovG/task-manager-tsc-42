package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.SessionDTO;
import ru.goloshchapov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.List;

public interface ITaskEndpoint {
    @WebMethod
    @SneakyThrows
    void addTaskAll(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "collection") Collection<TaskDTO> collection
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO addTask(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "task") TaskDTO model
    );

    @Nullable TaskDTO addTaskByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<TaskDTO> findTaskAll(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<TaskDTO> findTaskAllByUserId(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO findTaskById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String modelId
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO findTaskByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO findTaskByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    int sizeTask(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void removeTask(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "task") TaskDTO model
    );

    @WebMethod
    @SneakyThrows
    void clearTask(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO removeTaskById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String modelId
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO removeTaskByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO removeTaskByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO startTaskById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String modelId
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO startTaskByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO startTaskByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO finishTaskById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String modelId
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO finishTaskByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO finishTaskByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<TaskDTO> sortedTaskBy(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "sortCheck") String sortCheck);

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO updateTaskById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String modelId,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO updateTaskByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO changeTaskStatusById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String id,
            @WebParam(name = "statusChange") String statusChange
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO changeTaskStatusByName(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name,
            @WebParam(name = "statusChange") String statusChange
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO changeTaskStatusByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") int index,
            @WebParam(name = "statusChange") String statusChange
    );

    @WebMethod
    @SneakyThrows
    void checkTaskAccess(
            @WebParam(name = "session") SessionDTO session
    );
}
