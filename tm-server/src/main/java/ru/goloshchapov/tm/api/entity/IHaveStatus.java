package ru.goloshchapov.tm.api.entity;

import ru.goloshchapov.tm.enumerated.Status;

public interface IHaveStatus {

    Status getStatus();

    void setStatus(Status status);

}
