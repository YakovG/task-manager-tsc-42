package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.*;
import ru.goloshchapov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.List;

public interface IAdminEndpoint {
    @WebMethod
    @SneakyThrows
    void addUserAll(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "collection") Collection<UserDTO> collection
    );

    @WebMethod
    @SneakyThrows
    @Nullable UserDTO addUser(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "user") UserDTO entity
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<UserDTO> findUserAll(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @Nullable UserDTO findUserById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "userId") String id);

    @WebMethod
    @SneakyThrows
    @Nullable UserDTO findUserByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index);

    @WebMethod
    @SneakyThrows
    boolean isUserAbsentById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "userId") String id);

    @WebMethod
    @SneakyThrows
    boolean isUserAbsentByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index);

    @WebMethod
    @SneakyThrows
    String getUserIdByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index);

    void showAllUserWithProject(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void showUserList(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    int sizeUser(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void clearUser(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @Nullable UserDTO removeUserById(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "userId") String id
    );

    @WebMethod
    @SneakyThrows
    @Nullable UserDTO removeUserByIndex(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    boolean isUserLoginExists(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "login") String login);

    @WebMethod
    @SneakyThrows
    boolean isEmailExists(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "email") String email
    );

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO createUser(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password
    );

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO createUserWithEmail(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "email") String email
    );

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO createUserWithRole(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "role") Role role
    );

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO createUserWithAll(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "email") String email,
            @WebParam(name = "role") String role
    );

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO findUserByLogin(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "login") String login
    );

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO findUserByEmail(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "email") String email);

    @WebMethod
    @SneakyThrows
    @Nullable UserDTO removeUser(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "user") UserDTO user);

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO removeUserByLogin(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "login") String login);

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO removeUserByEmail(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "email") String email);

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO lockUserByLogin(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "login") String login
    );

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO unlockUserByLogin(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "login") String login);

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO lockUserByEmail(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "email") String email
    );

    @WebMethod
    @SneakyThrows
    @NotNull UserDTO unlockUserByEmail(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "email") String email
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO findProjectByNameWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    boolean isProjectAbsentByNameWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    String getProjectIdByNameWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name ") String name
    );

    @WebMethod
    @SneakyThrows
    void addProjectAllWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "collection") Collection<ProjectDTO> collection
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO addProjectWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "project") ProjectDTO entity
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<ProjectDTO> findProjectAllWithoutUserId(
            @WebParam(name = "session") SessionDTO session
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO findProjectByIdWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String id
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    ProjectDTO findProjectByIndexWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    boolean isProjectAbsentByIdWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String id
    );

    @WebMethod
    @SneakyThrows
    boolean isProjectAbsentByIndexWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    String getProjectIdByIndexWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    int sizeProjectWithoutUserId(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void clearProjectWithoutUserId(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void removeProjectWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "project") ProjectDTO entity
    );

    @WebMethod
    @SneakyThrows
    @Nullable ProjectDTO removeProjectByIdWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "projectId") String id
    );

    @WebMethod
    @SneakyThrows
    @Nullable ProjectDTO removeProjectByIndexWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO findTaskByNameWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    boolean isTaskAbsentByNameWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    String getTaskIdByNameWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "name") String name
    );

    @WebMethod
    @SneakyThrows
    void addTaskAllWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "collection") Collection<TaskDTO> collection
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO addTaskWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "task") TaskDTO entity
    );

    @WebMethod
    @SneakyThrows
    @Nullable List<TaskDTO> findTaskAllWithoutUserId(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO findTaskByIdWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String id
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO findTaskByIndexWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    boolean isTaskAbsentByIdWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String id
    );

    @WebMethod
    @SneakyThrows
    boolean isTaskAbsentByIndexWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    String getTaskIdByIndexWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "index") Integer index
    );

    @WebMethod
    @SneakyThrows
    int sizeTaskWithoutUserId(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void clearTaskWithoutUserId(
            @WebParam(name = "session") SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void removeTaskWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "task") TaskDTO entity
    );

    @WebMethod
    @SneakyThrows
    @Nullable TaskDTO removeTaskByIdWithoutUserId(
            @WebParam(name = "session") SessionDTO session,
            @WebParam(name = "taskId") String id
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO saveBase64Data(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO loadBase64Data(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO saveBinaryData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO loadBinaryData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO saveJsonFasterXmlData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO loadJsonFasterXmlData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO saveJsonJaxBData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO loadJsonJaxBData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO saveXmlFasterXmlData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO loadXmlFasterXmlData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO saveXmlJaxBData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO loadXmlJaxBData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO saveYamlData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO loadYamlData(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO saveBackup(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO loadBackup(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    @NotNull ResultDTO clearBackup(
            @WebParam(name = "session") @Nullable SessionDTO session
    );
}
