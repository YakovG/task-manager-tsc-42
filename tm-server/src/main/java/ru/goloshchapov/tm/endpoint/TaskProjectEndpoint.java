package ru.goloshchapov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.dto.ProjectDTO;
import ru.goloshchapov.tm.dto.SessionDTO;
import ru.goloshchapov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class TaskProjectEndpoint implements ru.goloshchapov.tm.api.endpoint.ITaskProjectEndpoint {

    private final ServiceLocator serviceLocator;

    public TaskProjectEndpoint (ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    @SneakyThrows
    @Nullable
    public List<TaskDTO> findAllTaskByProjectId(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isEmptyProjectWithTaskById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().isEmptyProjectById(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findAllTaskByProjectName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectName") final String projectName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllByProjectName(session.getUserId(), projectName);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isEmptyProjectWithTaskByName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectName") final String projectName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().isEmptyProjectByName(session.getUserId(), projectName);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findAllTaskByProjectIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectIndex") final Integer projectIndex
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllByProjectIndex(session.getUserId(), projectIndex);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isEmptyProjectWithTaskByIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectIndex") final Integer projectIndex
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().isEmptyProjectByIndex(session.getUserId(), projectIndex);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public TaskDTO bindTaskToProjectById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "taskId") final String taskId,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().bindToProjectById(session.getUserId(), taskId, projectId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public TaskDTO unbindTaskFromProjectById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "taskId") final String taskId,
            @WebParam(name = "projectId") String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().unbindFromProjectById(session.getUserId(), taskId, projectId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> removeAllTaskByProjectId(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeAllByProjectId(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> removeAllTaskByProjectName(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectName") final String projectName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeAllByProjectName(session.getUserId(), projectName);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> removeAllTaskByProjectIndex(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectIndex") final Integer projectIndex
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeAllByProjectIndex(session.getUserId(), projectIndex);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean removeAllTaskByUserId(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable ProjectDTO removeProjectByIdWithTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable ProjectDTO removeProjectByNameWithTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectName") final String projectName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeProjectByName(session.getUserId(), projectName);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable ProjectDTO removeProjectByIndexWithTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectIndex") final Integer projectIndex
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeProjectByIndex(session.getUserId(), projectIndex);
    }

}
