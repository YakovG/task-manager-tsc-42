package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.ISessionRepository;
import ru.goloshchapov.tm.api.service.ISessionService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.constant.RootUserConst;
import ru.goloshchapov.tm.dto.SessionDTO;
import ru.goloshchapov.tm.dto.UserDTO;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.util.SignatureUtil;

import java.util.List;

import static ru.goloshchapov.tm.util.HashUtil.salt;
import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class SessionService extends AbstractService<SessionDTO> implements ISessionService {

    @NotNull private final ServiceLocator serviceLocator;

    public SessionService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    private boolean checkContains(final int value) {
        return (value != 0);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<SessionDTO> getListSession(final SessionDTO session) {
        validate(session);
        final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAllSession();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findSessionByLogin(@Nullable final String login) {
        @NotNull final UserDTO user = serviceLocator.getUserService().findUserByLogin(login);
        final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final SessionDTO session = sessionRepository.findByUserId(user.getId());
            return session;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAllSession();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public final SessionDTO findOneById(@Nullable final String id) {
        if (id == null) throw new EmptyIdException();
        final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findSessionById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void closeSessionByLogin(@Nullable final String login, @Nullable final String password) {
        if (!checkDataAccess(login, password)) throw new AccessDeniedException();
        @Nullable final SessionDTO session = findSessionByLogin(login);
        close(session);
    }

    @Nullable
    @Override
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        @NotNull UserDTO user;
        if (RootUserConst.ROOT_LOGIN.equals(login))  user = serviceLocator.getUserService().getRootUser();
        else user = serviceLocator.getUserService().findUserByLogin(login);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String passwordHash = salt(propertyService, password);
        if (isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @NotNull UserDTO user;
        if (RootUserConst.ROOT_LOGIN.equals(login))  {
            user = serviceLocator.getUserService().getRootUser();
            serviceLocator.getUserService().add(user);
        }
        else user = serviceLocator.getUserService().findUserByLogin(login);
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final SessionDTO sessionSigned = sign(session);
            if (sessionSigned == null) throw new AccessDeniedException();
            if (!checkContains(sessionRepository.contains(session.getId()))) sessionRepository.add(session);
            sqlSession.commit();
            return sessionSigned;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO add(@Nullable SessionDTO session) {
        if (session == null) return null;
        final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(session);
            sqlSession.commit();
            return session;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void close(@Nullable SessionDTO session) {
        validate(session);
        final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.remove(session.getId());
            sqlSession.commit();
            final UserDTO user = serviceLocator.getUserService().getRootUser();
            if (user.getId().equals(session.getUserId()))
                serviceLocator.getUserService().findOneById(session.getUserId());
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO removeOneById(@Nullable String id) {
        if (id == null) throw new EmptyIdException();
        @Nullable final SessionDTO session = findOneById(id);
        if (session == null) throw new ElementsNotFoundException();
        close(session);
        return session;
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (!checkContains(sessionRepository.contains(session.getId()))) throw new AccessDeniedException();
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDTO session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = serviceLocator.getUserService().findUserById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }
}
