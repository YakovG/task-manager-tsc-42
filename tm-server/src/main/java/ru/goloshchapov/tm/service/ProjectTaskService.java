package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.api.service.IProjectTaskService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.dto.ProjectDTO;
import ru.goloshchapov.tm.dto.TaskDTO;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;

import java.util.Date;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.checkIndex;
import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull private final ServiceLocator serviceLocator;

    @NotNull
    public ProjectTaskService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO addProject(
            @Nullable final String userId,
            @Nullable final String projectName,
            @Nullable final String projectDescription
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(projectName);
        project.setDescription(projectDescription);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            project.setUserId(userId);
            projectRepository.add(project);
            sqlSession.commit();
            return project;
        } catch (final Exception e) {
            sqlSession.commit();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO addTask(
            @Nullable final String userId,
            @Nullable final String taskName,
            @Nullable final String taskDescription
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskName)) throw new EmptyNameException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(taskName);
        task.setDescription(taskDescription);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            task.setUserId(userId);
            taskRepository.add(task);
            sqlSession.commit();
            return task;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clearProjects(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.clear(userId);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAllProjects(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAll(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (serviceLocator.getProjectService().isAbsentById(projectId)) return null;
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmptyProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) return true;
        return findAllByProjectId(userId, projectId) == null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (serviceLocator.getProjectService().isAbsentByName(projectName)) return null;
            @Nullable final String projectId = serviceLocator.getProjectService().getIdByName(projectName);
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmptyProjectByName(@Nullable final String userId, @Nullable final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) return true;
        return findAllByProjectName(userId, projectName) == null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (projectIndex == null) throw new IndexIncorrectException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final int size = projectRepository.size();
            if (!checkIndex(projectIndex, size)) throw new IndexIncorrectException();
            if (serviceLocator.getProjectService().isAbsentByIndex(projectIndex)) return null;
            @Nullable String projectId = serviceLocator.getProjectService().getIdByIndex(projectIndex);
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmptyProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (projectIndex == null) throw new IndexIncorrectException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            final int size = projectRepository.size();
            if (!checkIndex(projectIndex, size)) return true;
            return findAllByProjectIndex(userId, projectIndex) == null;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO bindToProjectById(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskId) || isEmpty(projectId)) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (serviceLocator.getProjectService().isAbsentById(projectId)) return null;
            if (serviceLocator.getTaskService().isAbsentById(taskId)) return null;
            taskRepository.bindToProjectById(userId, taskId, projectId);
            @Nullable final TaskDTO task = serviceLocator.getTaskService().findOneById(userId, taskId);
            sqlSession.commit();
            return task;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO unbindFromProjectById(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskId) || isEmpty(projectId)) throw new EmptyIdException();
        if (serviceLocator.getProjectService().isAbsentById(projectId)) return null;
        if (serviceLocator.getTaskService().isAbsentById(taskId)) return null;
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.unbindFromProjectById(userId, taskId, null);
            @Nullable final TaskDTO task = serviceLocator.getTaskService().findOneById(userId, taskId);
            sqlSession.commit();
            return task;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        @Nullable final List<TaskDTO> tasks = findAllByProjectId(userId, projectId);
        if (tasks == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (serviceLocator.getProjectService().isAbsentById(projectId)) return null;
            taskRepository.removeAllByProjectId(userId, projectId);
            sqlSession.commit();
            return tasks;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> removeAllByProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        @Nullable final List<TaskDTO> tasks = findAllByProjectName(userId, projectName);
        if (tasks == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (serviceLocator.getProjectService().isAbsentByName(projectName)) return null;
            @Nullable final String projectId = serviceLocator.getProjectService().getIdByName(projectName);
            taskRepository.removeAllByProjectId(userId, projectId);
            sqlSession.commit();
            return tasks;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> removeAllByProjectIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (projectIndex == null) throw new IndexIncorrectException();
        @Nullable final List<TaskDTO> tasks = findAllByProjectIndex(userId, projectIndex);
        if (tasks == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final int size = projectRepository.size();
            if (!checkIndex(projectIndex, size)) throw new IndexIncorrectException();
            if (serviceLocator.getProjectService().isAbsentByIndex(projectIndex)) return null;
            @Nullable final String projectId = serviceLocator.getProjectService().getIdByIndex(projectIndex);
            taskRepository.removeAllByProjectId(userId, projectId);
            sqlSession.commit();
            return tasks;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean removeAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final List<ProjectDTO> projects = projectRepository.findAllByUserId(userId);
            if (projects == null || projects.isEmpty()) return false;
            for (@NotNull final ProjectDTO project : projects) {
                removeProjectById(userId, project.getId());
            }
            sqlSession.commit();
            return true;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        @Nullable final ProjectDTO project = serviceLocator.getProjectService().findOneById(userId, projectId);
        if (project == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (serviceLocator.getProjectService().isAbsentById(projectId)) return null;
            if (!isEmptyProjectById(userId, projectId)) removeAllByProjectId(userId, projectId);
            projectRepository.removeOneById(userId, projectId);
            sqlSession.commit();
            return project;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeProjectByName(@Nullable final String userId, @Nullable final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        if (serviceLocator.getProjectService().isAbsentByName(projectName)) return null;
        if (!isEmptyProjectByName(userId, projectName)) removeAllByProjectName(userId, projectName);
        @Nullable final ProjectDTO project = serviceLocator.getProjectService().removeOneByName(userId, projectName);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (projectIndex == null) throw new IndexIncorrectException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            final int size = projectRepository.size();
            if (!checkIndex(projectIndex, size)) throw new IndexIncorrectException();
            if (serviceLocator.getProjectService().isAbsentByIndex(projectIndex)) return null;
            if (!isEmptyProjectByIndex(userId, projectIndex)) removeAllByProjectIndex(userId, projectIndex);
            @Nullable final ProjectDTO project = serviceLocator.getProjectService().removeOneByIndex(userId, projectIndex);
            return project;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void createTestData() {
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            projectRepository.clearAll();
            taskRepository.clearAll();
            @NotNull final Date date = new Date();
            final long time = date.getTime();
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setId("ppp111");
            project.setUserId("ddd");
            project.setName("Project_1");
            project.setDescription("About_1");
            project.setStatus(Status.COMPLETE);
            project.setDateStart(date);
            project.setDateFinish(new Date(time + 300000));
            projectRepository.add(project);
            @NotNull final ProjectDTO project1 = new ProjectDTO();
            project1.setId("ppp222");
            project1.setUserId("ttt");
            project1.setName("Project_2");
            project1.setDescription("About_2");
            projectRepository.add(project1);
            @NotNull final ProjectDTO project2 = new ProjectDTO();
            project2.setId("ppp333");
            project2.setUserId("ttt");
            project2.setName("Project_3");
            project2.setDescription("About_3");
            project2.setStatus(Status.IN_PROGRESS);
            project2.setDateStart(new Date(time + 30000));
            projectRepository.add(project2);
            @NotNull final ProjectDTO project3 = new ProjectDTO();
            project3.setId("ppp444");
            project3.setUserId("ddd");
            project3.setName("Project_4");
            project3.setDescription("About_4");
            projectRepository.add(project3);
            @NotNull final ProjectDTO project4 = new ProjectDTO();
            project4.setId("ppp555");
            project4.setUserId("aaa");
            project4.setName("Project_5");
            project4.setDescription("About_5");
            project4.setStatus(Status.IN_PROGRESS);
            project4.setDateStart(new Date(time + 40000));
            projectRepository.add(project4);
            sqlSession.commit();
            @NotNull final TaskDTO task = new TaskDTO();
            task.setId("11111");
            task.setUserId("ddd");
            task.setName("Task_1");
            task.setDescription("Desc_1");
            task.setStatus(Status.IN_PROGRESS);
            task.setDateStart(new Date(time + 60000));
            taskRepository.add(task);
            @NotNull final TaskDTO task1 = new TaskDTO();
            task1.setId("22222");
            task1.setUserId("ddd");
            task1.setName("Task_2");
            task1.setDescription("Desc_2");
            task1.setStatus(Status.NOT_STARTED);
            taskRepository.add(task1);
            @NotNull final TaskDTO task2 = new TaskDTO();
            task2.setId("33333");
            task2.setUserId("ttt");
            task2.setName("Task_3");
            task2.setDescription("Desc_3");
            task2.setStatus(Status.COMPLETE);
            task2.setDateStart(new Date());
            task2.setDateFinish(new Date(time + 60000));
            taskRepository.add(task2);
            @NotNull final TaskDTO task3 = new TaskDTO();
            task3.setId("44444");
            task3.setUserId("ttt");
            task3.setName("Task_4");
            task3.setDescription("Desc_4");
            task3.setStatus(Status.IN_PROGRESS);
            task3.setDateStart(new Date(time + 120000));
            taskRepository.add(task3);
            @NotNull final TaskDTO task4 = new TaskDTO();
            task4.setId("55555");
            task4.setUserId("ttt");
            task4.setName("Task_5");
            task4.setDescription("Desc_5");
            task4.setStatus(Status.NOT_STARTED);
            taskRepository.add(task4);
            @NotNull final TaskDTO task5 = new TaskDTO();
            task5.setId("66666");
            task5.setUserId("ttt");
            task5.setName("Task_6");
            task5.setDescription("Desc_6");
            task5.setStatus(Status.COMPLETE);
            task5.setDateStart(new Date(time + 30000));
            task5.setDateFinish(new Date(time + 270000));
            taskRepository.add(task5);
            @NotNull final TaskDTO task6 = new TaskDTO();
            task6.setId("77777");
            task6.setUserId("ddd");
            task6.setName("Task_7");
            task6.setDescription("Desc_7");
            task6.setStatus(Status.COMPLETE);
            task6.setDateStart(new Date(time + 300000));
            task6.setDateFinish(new Date(time + 1200000));
            taskRepository.add(task6);
            @NotNull final TaskDTO task7 = new TaskDTO();
            task7.setId("88888");
            task7.setUserId("ddd");
            task7.setName("Task_8");
            task7.setDescription("Desc_8");
            task7.setStatus(Status.IN_PROGRESS);
            task7.setDateStart(new Date(time + 3000));
            taskRepository.add(task7);
            @NotNull final TaskDTO task8 = new TaskDTO();
            task8.setId("99999");
            task8.setUserId("aaa");
            task8.setName("Task_9");
            task8.setDescription("Desc_9");
            task8.setStatus(Status.NOT_STARTED);
            taskRepository.add(task8);
            @NotNull final TaskDTO task9 = new TaskDTO();
            task9.setId("00000");
            task9.setUserId("aaa");
            task9.setName("Task_0");
            task9.setDescription("Desc_0");
            task9.setStatus(Status.IN_PROGRESS);
            task9.setDateStart(new Date(time + 330000));
            taskRepository.add(task9);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        @NotNull final SqlSession sqlSession1 = serviceLocator.getConnectionService().getSqlSession();
        try {
            bindToProjectById("ddd", "11111", "ppp111");
            bindToProjectById("ddd", "22222", "ppp111");
            bindToProjectById("ttt", "33333", "ppp222");
            bindToProjectById("ttt", "44444", "ppp222");
            bindToProjectById("ttt", "55555", "ppp333");
            bindToProjectById("ttt", "66666", "ppp333");
            bindToProjectById("ddd", "77777", "ppp444");
            bindToProjectById("ddd", "88888", "ppp444");
            bindToProjectById("aaa", "99999", "ppp555");
            bindToProjectById("aaa", "00000", "ppp555");
            sqlSession1.commit();
        } catch (final Exception e) {
            sqlSession1.rollback();
            throw e;
        } finally {
            sqlSession1.close();
        }
    }

}
