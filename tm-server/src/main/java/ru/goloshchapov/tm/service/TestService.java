package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.*;
import ru.goloshchapov.tm.api.service.ITestService;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;

public final class TestService implements ITestService, ITestRepository {

    private final IPropertyService propertyService;

    private final SqlSessionFactory sqlSessionFactory;

    public TestService(IPropertyService propertyService) {
        this.propertyService = propertyService;
        sqlSessionFactory = getSqlSessionFactory();
    }

    @Override
    @NotNull
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String url = propertyService.getJdbcTestUrl();
        @NotNull final String driver = propertyService.getJdbcDriver();
        @NotNull final String user = propertyService.getJdbcTestUsername();
        @NotNull final String password = propertyService.getJdbcTestPassword();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITestRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }


    @Override
    @SneakyThrows
    public void initTestUserTable() {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final ITestRepository testRepository = sqlSession.getMapper(ITestRepository.class);
            testRepository.initTestUserTable();
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void initTestSessionTable() {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final ITestRepository testRepository = sqlSession.getMapper(ITestRepository.class);
            testRepository.initTestSessionTable();
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void initTestProjectTable() {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final ITestRepository testRepository = sqlSession.getMapper(ITestRepository.class);
            testRepository.initTestProjectTable();
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void initTestTaskTable() {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final ITestRepository testRepository = sqlSession.getMapper(ITestRepository.class);
            testRepository.initTestTaskTable();
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void dropDatabase() {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final ITestRepository testRepository = sqlSession.getMapper(ITestRepository.class);
            testRepository.dropDatabase();
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }
}
