package ru.goloshchapov.tm;

import org.junit.Assert;
import org.junit.Test;
import ru.goloshchapov.tm.endpoint.ResultDTO;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.SessionEndpoint;
import ru.goloshchapov.tm.endpoint.SessionEndpointService;

import static ru.goloshchapov.tm.constant.ResultConst.RESULT_FAIL;
import static ru.goloshchapov.tm.constant.ResultConst.RESULT_SUCCESS;

public class SessionEndpointTest {

    private final SessionEndpointService sessionEndpointService =
            new SessionEndpointService();

    private final SessionEndpoint sessionEndpoint =
            sessionEndpointService.getSessionEndpointPort();

    @Test
    public void testOpenSession() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
    }

    @Test
    public void testOpenSessionIncorrect() {
        final SessionDTO session = sessionEndpoint.openSession("test123", "test123");
    }

    @Test
    public void testCloseSession() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
        final ResultDTO result = sessionEndpoint.closeSession(session);
        Assert.assertNotNull(result);
        Assert.assertEquals(RESULT_SUCCESS, result.getResult());
    }

    @Test
    public void testCloseSessionByLogin() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
        final ResultDTO result = sessionEndpoint.closeSessionByLogin("test", "test");
        Assert.assertNotNull(result);
        Assert.assertEquals(RESULT_SUCCESS, result.getResult());
    }

    @Test
    public void testCloseSessionByLoginIncorrect() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
        final ResultDTO result = sessionEndpoint.closeSessionByLogin("test123", "test123");
        Assert.assertNotNull(result);
        Assert.assertEquals(RESULT_FAIL, result.getResult());
    }

    @Test
    public void testRootSession() {
        final SessionDTO session = sessionEndpoint.openSession("root", "root");
        Assert.assertNotNull(session);
        final ResultDTO result = sessionEndpoint.closeSessionByLogin("root", "root");
        Assert.assertNotNull(result);
        Assert.assertEquals(RESULT_SUCCESS, result.getResult());
    }
}
