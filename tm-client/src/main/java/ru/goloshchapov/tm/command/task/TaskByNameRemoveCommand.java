package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.TaskDTO;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByNameRemoveCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-remove-by-name";

    @NotNull public static final String DESCRIPTION = "Remove task by name";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = endpointLocator.getTaskEndpoint().removeTaskByName(session, name);
        if (task == null) throw new TaskNotFoundException();
    }
}
