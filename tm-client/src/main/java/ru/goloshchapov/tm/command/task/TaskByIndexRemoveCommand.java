package ru.goloshchapov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.TaskDTO;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByIndexRemoveCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-remove-by-index";

    @NotNull public static final String DESCRIPTION = "Remove task by index";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        @Nullable final TaskDTO task = endpointLocator.getTaskEndpoint().removeTaskByIndex(session, index);
        if (task == null) throw new TaskNotFoundException();
    }
}
