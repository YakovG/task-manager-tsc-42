package ru.goloshchapov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.UserDTO;

import java.util.List;

public final class UserAndProjectListCommand extends AbstractCommand {

    @NotNull public static final String NAME = "user-project-list";

    @NotNull public static final String DESCRIPTION = "Show all users with project-list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[USERS WITH PROJECT LIST]");
        endpointLocator.getAdminEndpoint().showAllUserWithProject(session);
        @Nullable final List<UserDTO> users = endpointLocator.getAdminEndpoint().findUserAll(session);
        for (@NotNull final UserDTO user:users) {
            System.out.println(user.getLogin() + "   " + user.getRole());
            endpointLocator.getAdminEndpoint().findProjectAllWithoutUserId(session)
                    .stream().filter(project -> project.getUserId().equals(user.getId()))
                    .forEach(p -> System.out.println(p.toString()));
            System.out.println("--------------------------------");
        }
    }

}
