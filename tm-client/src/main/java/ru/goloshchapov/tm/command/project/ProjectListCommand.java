package ru.goloshchapov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.ProjectDTO;
import ru.goloshchapov.tm.endpoint.SessionDTO;

import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-list";

    @NotNull public static final String DESCRIPTION = "Show project list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[PROJECT LIST]");
        @NotNull final List<ProjectDTO> projects = endpointLocator.getProjectEndpoint().findAllProjectByUserId(session);
        int index = 1;
        for (@NotNull final ProjectDTO project: projects) {
            System.out.println(index + ". " + project.getId() + " : " + project.getName());
            index++;
        }
    }
}
