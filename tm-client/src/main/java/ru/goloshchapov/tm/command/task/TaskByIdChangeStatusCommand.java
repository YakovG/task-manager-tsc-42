package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.SessionDTO;
import ru.goloshchapov.tm.endpoint.TaskDTO;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.entity.TaskNotUpdatedException;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskByIdChangeStatusCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-change-status-by-id";

    @NotNull public static final String DESCRIPTION = "Change task status by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSession();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        @NotNull final Status[] statuses = Status.values();
        System.out.println(Arrays.toString(statuses));
        @Nullable final String statusChange = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = endpointLocator.getTaskEndpoint()
                .changeTaskStatusById(session, taskId, statusChange);
        if (task == null) throw new TaskNotUpdatedException();
    }
}
