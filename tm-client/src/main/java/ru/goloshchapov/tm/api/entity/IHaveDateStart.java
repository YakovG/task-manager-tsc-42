package ru.goloshchapov.tm.api.entity;

import java.util.Date;

public interface IHaveDateStart {

    Date getDateStart ();

    void setDateStart (Date date);

}
