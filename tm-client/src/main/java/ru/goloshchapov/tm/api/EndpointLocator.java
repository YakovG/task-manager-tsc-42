package ru.goloshchapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.endpoint.*;

public interface EndpointLocator extends ServiceLocator {

    @NotNull CalculatorEndpoint getCalculatorEndpoint();

    @NotNull ProjectEndpoint getProjectEndpoint();

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull TaskProjectEndpoint getTaskProjectEndpoint();

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull UserEndpoint getUserEndpoint();

    @NotNull AdminEndpoint getAdminEndpoint();

    void setSession(@Nullable final SessionDTO session);

    @Nullable SessionDTO getSession();

}
