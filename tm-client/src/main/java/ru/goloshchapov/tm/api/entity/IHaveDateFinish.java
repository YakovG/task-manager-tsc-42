package ru.goloshchapov.tm.api.entity;

import java.util.Date;

public interface IHaveDateFinish {

    Date getDateFinish ();

    void setDateFinish (Date date);

}
