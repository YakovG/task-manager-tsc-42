package ru.goloshchapov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull String getApplicationVersion();

    @NotNull String getServerHost();

    @NotNull String getServerPort();

    @NotNull String getSessionSalt();

    @NotNull Integer getSessionCycle();
}
